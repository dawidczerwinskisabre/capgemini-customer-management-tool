﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapgeminiMVC.ViewModels
{
    public class CustomerViewModel
    {
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public long? TelephoneNo { get; set; }
        public int AddressId { get; set; }
        public string Street { get; set; }
        public int? HouseNo { get; set; }
        public int? FlatNo { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
    }
}