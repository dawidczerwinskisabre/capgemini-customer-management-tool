﻿using CapgeminiMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapgeminiMVC.ViewModels
{
    public class CustomerListViewModel
    {
        public ICollection<CustomerViewModel> Customers { get; set; }
    }
}