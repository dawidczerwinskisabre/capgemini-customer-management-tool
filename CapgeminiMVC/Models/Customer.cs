﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CapgeminiMVC.Models
{
    public class Customer
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public long? TelephoneNo { get; set; }
        [ForeignKey ( "Address" )]
        public int AddressId { get; set; }
        public virtual Address Address { get; set; }
    }
}