﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace CapgeminiMVC.Models
{
    public interface ICapgeminiMVCDbContext
    {
        DbSet<Customer> Customers { get; set; }
        DbSet<Address> Addresses { get; set; }
    }

    public class CapgeminiMVCDbContext : System.Data.Entity.DbContext, ICapgeminiMVCDbContext
    {
        public CapgeminiMVCDbContext () : base ( "CapgeminiDb" )
        {

        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Address> Addresses { get; set; }

        protected override void OnModelCreating ( DbModelBuilder modelBuilder )
            => modelBuilder.Conventions.Remove<PluralizingTableNameConvention> ();

    }
}