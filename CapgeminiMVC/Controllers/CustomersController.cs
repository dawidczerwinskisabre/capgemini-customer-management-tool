﻿using CapgeminiMVC.Models;
using CapgeminiMVC.Services;
using CapgeminiMVC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Web.Routing;

namespace CapgeminiMVC.Controllers
{
    public class CustomersController : Controller
    {
        private ICustomerService _customerService { get; set; }

        public CustomersController ( ICustomerService customerService )
        {
            _customerService = customerService;
        }

        [HttpGet]
        public ActionResult Index ()
        {
            var customers = _customerService.GetCustomers ();
            var vm = new CustomerListViewModel ()
            {
                Customers = customers
            };

            return View ( vm );
        }

        [HttpGet]
        public ActionResult GetCustomers ( List<int> ids = null )
        {
            var customers = _customerService.GetCustomers ();
            return Json ( new { data = customers }, JsonRequestBehavior.AllowGet );
        }

        [HttpGet]
        public ActionResult GetCustomer ( int customerId )
        {
            var customer = _customerService.GetCustomerById ( customerId );
            return Json ( new { data = customer }, JsonRequestBehavior.AllowGet );
        }

        [HttpPost]
        public ActionResult SaveCustomer ( string inputName, string inputSurname, string inputEmail, long? inputTelephone,
            string inputStreet, int? inputHouseNo, int? inputFlatNo, string inputCity, string inputPostalCode, string inputRegion, string inputCountry, int? inputCustomerId, int? inputAddressId )
        {

            var customer = new CustomerViewModel ()
            {
                Name = inputName,
                Surname = inputSurname,
                Email = inputEmail,
                TelephoneNo = inputTelephone,
                Street = inputStreet,
                HouseNo = inputHouseNo,
                FlatNo = inputFlatNo,
                City = inputCity,
                PostalCode = inputPostalCode,
                Region = inputRegion,
                Country = inputCountry
            };

            if ( inputCustomerId.HasValue )
            {
                customer.CustomerId = inputCustomerId.Value;
            }

            if ( inputAddressId.HasValue )
            {
                customer.AddressId = inputAddressId.Value;
            }

            var success = _customerService.SaveCustomer ( customer );

            return RedirectToAction ( "Index" );
        }

        [HttpPost]
        public ActionResult DeleteCustomer ( int customerId )
        {
            var success = _customerService.DeleteCustomer ( customerId );

            return RedirectToAction ( "Index" );
        }

    }
}