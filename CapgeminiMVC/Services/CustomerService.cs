﻿using CapgeminiMVC.Models;
using CapgeminiMVC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapgeminiMVC.Services
{
    public interface ICustomerService
    {
        List<CustomerViewModel> GetCustomers ( List<int> ids = null );
        CustomerViewModel GetCustomerById ( int id );
        bool SaveCustomer ( CustomerViewModel customer );
        bool DeleteCustomer ( int customerId );
    }

    public class CustomerService : ICustomerService
    {
        private CapgeminiMVCDbContext _dbContext { get; set; }

        public CustomerService ( CapgeminiMVCDbContext dbContext )
        {
            _dbContext = dbContext;
        }

        public List<CustomerViewModel> GetCustomers ( List<int> ids = null )
        {
            var result = new List<CustomerViewModel> ();
            var dbCustomers = ids == null ? _dbContext.Customers.ToList () :
                _dbContext.Customers.Where ( x => ids.Contains ( x.Id ) ).ToList ();

            foreach ( var _customer in dbCustomers )
            {
                var customer = new CustomerViewModel ();

                MapDbEntityToViewModel ( _customer, ref customer );

                result.Add ( customer );
            }

            return result;
        }

        public CustomerViewModel GetCustomerById ( int id )
        {
            var customer = new CustomerViewModel ();
            var dbCustomer = _dbContext.Customers.Where ( x => x.Id == id ).SingleOrDefault ();

            MapDbEntityToViewModel ( dbCustomer, ref customer );

            return customer;
        }

        public bool SaveCustomer ( CustomerViewModel customer )
        {
            bool success = false;

            try
            {
                var dbCustomer = _dbContext.Customers.Where ( x => x.Id == customer.CustomerId ).SingleOrDefault ();

                if ( dbCustomer != null )
                {
                    MapViewModelToDbEntity ( customer, ref dbCustomer );
                }
                else
                {
                    dbCustomer = new Customer ();
                    dbCustomer.Address = new Address ();
                    MapViewModelToDbEntity ( customer, ref dbCustomer );
                    _dbContext.Customers.Add ( dbCustomer );
                }

                _dbContext.SaveChanges ();
                success = true;
            }
            catch ( Exception ex )
            {
                //TODO Logging
            }

            return success;
        }

        public bool DeleteCustomer ( int customerId )
        {
            bool success = false;

            try
            {
                var dbCustomer = _dbContext.Customers.Where ( x => x.Id == customerId ).SingleOrDefault ();

                if ( dbCustomer != null )
                {
                    var dbAddress = _dbContext.Addresses.Where ( x => x.Id == dbCustomer.Address.Id ).SingleOrDefault ();
                    _dbContext.Customers.Remove ( dbCustomer );
                    _dbContext.Addresses.Remove ( dbAddress );
                }

                _dbContext.SaveChanges ();
                success = true;
            }
            catch ( Exception ex )
            {
                //Logging
            }

            return success;
        }

        private void MapViewModelToDbEntity ( CustomerViewModel viewModel, ref Customer dbEntity )
        {
            dbEntity.Id = viewModel.CustomerId;
            dbEntity.Name = viewModel.Name;
            dbEntity.Surname = viewModel.Surname;
            dbEntity.Email = viewModel.Email;
            dbEntity.TelephoneNo = viewModel.TelephoneNo;
            dbEntity.Address.Id = viewModel.AddressId;
            dbEntity.Address.Street = viewModel.Street;
            dbEntity.Address.HoueNo = viewModel.HouseNo;
            dbEntity.Address.FlatNo = viewModel.FlatNo;
            dbEntity.Address.City = viewModel.City;
            dbEntity.Address.PostalCode = viewModel.PostalCode;
            dbEntity.Address.Region = viewModel.Region;
            dbEntity.Address.Country = viewModel.Country;
        }

        private void MapDbEntityToViewModel ( Customer dbEntity, ref CustomerViewModel viewModel )
        {
            viewModel.CustomerId = dbEntity.Id;
            viewModel.Name = dbEntity.Name;
            viewModel.Surname = dbEntity.Surname;
            viewModel.Email = dbEntity.Email;
            viewModel.TelephoneNo = dbEntity.TelephoneNo;
            viewModel.AddressId = dbEntity.Address.Id;
            viewModel.Street = dbEntity.Address.Street;
            viewModel.HouseNo = dbEntity.Address.HoueNo;
            viewModel.FlatNo = dbEntity.Address.FlatNo;
            viewModel.City = dbEntity.Address.City;
            viewModel.PostalCode = dbEntity.Address.PostalCode;
            viewModel.Region = dbEntity.Address.Region;
            viewModel.Country = dbEntity.Address.Country;
        }
    }
}