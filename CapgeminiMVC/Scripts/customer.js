﻿'use-strict';

Vue.use(Vuetable);
new Vue({
    el: '#app',
    data() {
        return {
            fields: [
                { name: 'CustomerId', title: 'ID' },
                { name: 'Name' },
                { name: 'Surname' },
                { name: 'Email' },
                { name: 'TelephoneNo', title: 'Telephone Number' },
                { name: 'Street' },
                { name: 'HouseNo', title: 'House Number' },
                { name: 'FlatNo', title: 'Flat Number' },
                { name: 'City' },
                { name: 'PostalCode', title: 'Postal Code' },
                { name: 'Region' },
                { name: 'Country' },
                {
                    name: '__slot:edit',
                    title: ' ',
                },
                {
                    name: '__slot:delete',
                    title: ' ',
                }],
        }
    },
    methods: {
        onPaginationData(paginationData) {
            this.$refs.pagination.setPaginationData(paginationData)
        },
        onChangePage(page) {
            this.$refs.vuetable.changePage(page)
        }
    }
})

function addCustomer() {
    $('#add-customer-modal').modal('show');
};

function closeCustomerModal() {
    document.getElementById("add-customer-form").reset();
    $('#inputName').css('border', '1px solid #ccc');
    $('#inputSurname').css('border', '1px solid #ccc');
    $('#inputEmail').css('border', '1px solid #ccc');
};

function editCustomer(customerId) {

    $.get("/Customers/GetCustomer/?customerId=" + customerId, function (data) {
        document.forms["edit-customer-form"]["inputName"].value = data.data.Name;
        document.forms["edit-customer-form"]["inputSurname"].value = data.data.Surname;
        document.forms["edit-customer-form"]["inputEmail"].value = data.data.Email;
        document.forms["edit-customer-form"]["inputTelephone"].value = data.data.TelephoneNo;
        document.forms["edit-customer-form"]["inputStreet"].value = data.data.Street;
        document.forms["edit-customer-form"]["inputHouseNo"].value = data.data.HouseNo;
        document.forms["edit-customer-form"]["inputFlatNo"].value = data.data.FlatNo;
        document.forms["edit-customer-form"]["inputCity"].value = data.data.City;
        document.forms["edit-customer-form"]["inputPostalCode"].value = data.data.PostalCode
        document.forms["edit-customer-form"]["inputRegion"].value = data.data.Region;
        document.forms["edit-customer-form"]["inputCountry"].value = data.data.Country;
        document.forms["edit-customer-form"]["inputCustomerId"].value = data.data.CustomerId;
        document.forms["edit-customer-form"]["inputAddressId"].value = data.data.AddressId;

        $('#edit-customer-modal').modal('show');
    });
};

function closeEditCustomerModal() {
    document.getElementById("edit-customer-form").reset();
    $('#inputName').css('border', '1px solid #ccc');
    $('#inputSurname').css('border', '1px solid #ccc');
    $('#inputEmail').css('border', '1px solid #ccc');
};

function removeCustomer(customerId) {
    $.post("/Customers/DeleteCustomer/?customerId=" + customerId);
    window.location.href = "http://capgeminicmt.azurewebsites.net/";
};

/* Validation */
function validateCustomerForm() {
    var name = validateName();
    var surname = validateSurname();
    var email = validateEmail();

    if (name && surname && email) {
        document.getElementById("add-customer-form").submit();
    }
};

function validateName() {
    var name = document.forms["add-customer-form"]["inputName"].value;
    if (name == "") {
        $('#inputName').css('border', '1px solid rgb(175,0,0)');
        return false;
    } else {
        $('#inputName').css('border', '1px solid #ccc');
        return true;
    }
};

function validateSurname() {
    var surname = document.forms["add-customer-form"]["inputSurname"].value;

    if (surname == "") {
        $('#inputSurname').css('border', '1px solid rgb(175,0,0)');
        return false;
    } else {
        $('#inputSurname').css('border', '1px solid #ccc');
        return true;
    }
}

function validateEmail() {
    var email = document.forms["add-customer-form"]["inputEmail"].value;
    var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var isEmailValid = pattern.test(email);

    if (!isEmailValid) {
        $('#inputEmail').css('border', '1px solid rgb(175,0,0)');
        return false;
    } else {
        $('#inputEmail').css('border', '1px solid #ccc');
        return true;
    }
}

function validateEditCustomerForm() {
    var name = validateEditName();
    var surname = validateEditSurname();
    var email = validateEditEmail();

    if (name && surname && email) {
        document.getElementById("edit-customer-form").submit();
    }
};

function validateEditName() {
    var name = document.forms["edit-customer-form"]["inputName"].value;
    if (name == "") {
        $('#inputEditName').css('border', '1px solid rgb(175,0,0)');
        return false;
    } else {
        $('#inputEditName').css('border', '1px solid #ccc');
        return true;
    }
};

function validateEditSurname() {
    var surname = document.forms["edit-customer-form"]["inputSurname"].value;

    if (surname == "") {
        $('#inputEditSurname').css('border', '1px solid rgb(175,0,0)');
        return false;
    } else {
        $('#inputEditSurname').css('border', '1px solid #ccc');
        return true;
    }
}

function validateEditEmail() {
    var email = document.forms["edit-customer-form"]["inputEmail"].value;
    var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var isEmailValid = pattern.test(email);

    if (!isEmailValid) {
        $('#inputEditEmail').css('border', '1px solid rgb(175,0,0)');
        return false;
    } else {
        $('#inputEditEmail').css('border', '1px solid #ccc');
        return true;
    }
}