/* Sidebar */

$(document).ready(function () {
    $("#menu-list-toggle-max").click(function (e) {
        e.preventDefault();
        $('.main-area').addClass('main-area-min');
        $('.side-bar').hide();
        $('.side-bar-min').show();
    });

    $("#menu-list-toggle-min").click(function (e) {
        e.preventDefault();
        $('.main-area').removeClass('main-area-min');
        $('.side-bar').show();
        $('.side-bar-min').hide();
    });
});


// filter items when filter link is clicked
$('#filters a').click(function () {
    var selector = $(this).attr('data-filter');
    $container.isotope({ filter: selector });
    return false;
});

/* Navigation (Select box) */

// Create the dropdown base
$("<select />").appendTo(".navis");

// Create default option "Go to..."
$("<option />", {
    "selected": "selected",
    "value": "",
    "text": "Menu"
}).appendTo(".navis select");

// Populate dropdown with menu items
$(".navi a").each(function () {
    var el = $(this);
    $("<option />", {
        "value": el.attr("href"),
        "text": el.text()
    }).appendTo(".navis select");
});

$(".navis select").change(function () {
    window.location = $(this).find("option:selected").val();
});

/* Moving sidebar below in small screens. */

$('.sidey').clone().appendTo('.mobily');


/* *************************************** */
/* Scroll to Top */
/* *************************************** */

$(document).ready(function () {
    $(".totop").hide();

    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('.totop').fadeIn();
        } else {
            $('.totop').fadeOut();
        }
    });
    $(".totop a").click(function (e) {
        e.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

});
/* *************************************** */

